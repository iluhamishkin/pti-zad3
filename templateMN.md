
---
author: Illia Mishkin
title: Daria Zawiałów
subtitle: czyli mój ulubiony muzykant
date: 2020-10-24
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Biografia

**Daria Zawiałow** - polska piosenkarka, autorka tekstów i kompozytorka.

![alt Daria Zawiałów](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Daria_Zawialow_2019.jpg/160px-Daria_Zawialow_2019.jpg) { width: 20%; } 

## Młodość

Jako nastolatka uczęszczała na zajęcia muzyczne w Mini Studiu Poezji i Piosenki w koszalińskim Centrum Kultury. Po ukończeniu gimnazjum wyjechała do Warszawy, aby tam od września 2008 podjąć naukę w liceum o profilu muzycznym. Absolwentka stołecznego LXXV Liceum Ogólnokształcącego im. Jana III Sobieskiego

## Kariera muzyczna - 1

W **2000** wystąpiła w programie TVP1 Od przedszkola do Opola, gdzie zaśpiewała piosenkę „Rzeka” z repertuaru Wolnej Grupy Bukowina. Brała udział w wielu konkursach i festiwalach dziecięcych i młodzieżowych. Na swoje utrzymanie pracowała z zespołem, grając na weselach.

## Kariera muzyczna - 2

W **2007** zdobyła nagrodę Tukan Junior za pierwsze miejsce w konkursie Dziecięcego Konkursu Aktorskiej Interpretacji Piosenki w ramach 28. Przeglądu *Piosenki Aktorskiej (PPA)* we Wrocławiu. W **2009** zwyciężyła w programie Szansa na sukces, dzięki czemu wzięła udział w koncercie „Debiuty” na Festiwalu w Opolu, gdzie wykonała piosenkę „Era retuszera” Kasi Nosowskiej. Po udziale w programie dowiedziała się o przesłuchaniach do chórków Maryli Rodowicz; ostatecznie wygrała castingi, a w chórkach artystki śpiewała przez kolejne dwa lata.

## Kariera muzyczna - 3

W **2011** wzięła udział w czwartym sezonie programu TVN Mam talent!. Dotarła do półfinału. W **2012**, pod pseudonimem D.A.R.I.A., opublikowała utwór „Half Way to Heaven”. W **2013** i **2014** wzięła udział w trzeciej i czwartej edycji programu X Factor, w tej ostatniej dotarła do finałowej dwunastki. W październiku 2014 wygrała konkurs Pejzaż bez Ciebie, dwa lata później (wiosną **2016**) zwyciężyła w konkursie Byłaś serca biciem. W tym samym roku wygrała Przegląd Piosenki i Ballady Filmowej w Toruniu.

## Dyskografia

**2017 - A kysz!**

* Wydany: 3 marca 2017
* Wytwórnia: [Sony Music](https://pl.wikipedia.org/wiki/Sony_Music_Entertainment_Poland)
* Format: [CD](https://pl.wikipedia.org/wiki/P%C5%82yta_kompaktowa), [digital download](https://pl.wikipedia.org/wiki/Digital_download)

**2019 - Helsinki**

* Wydany: 8 marca 2019
* Wytwórnia: [Sony Music](https://pl.wikipedia.org/wiki/Sony_Music_Entertainment_Poland)
* Format: [CD](https://pl.wikipedia.org/wiki/P%C5%82yta_kompaktowa), (LP)[https://pl.wikipedia.org/wiki/P%C5%82yta_gramofonowa], [digital download](https://pl.wikipedia.org/wiki/Digital_download)

## Teledyski


| Rok             | Nazwa                            |
| :-------------: |:--------------------------------:|
| 2016            | „Malinowy chruśniak”             |
|                 | „Kundel bury”                    |
| 2017            | „Miłostki”                       |
|                 | „Na skróty”                      |
|                 | „Jeszcze w zielone gramy”        |
| 2020            | „Helsinki”                       |
|                 | „Świt”                           |

## Nagrody i nominacje

**2020**

1. [Fryderyki 2020](https://pl.wikipedia.org/wiki/Fryderyki_2020)
2. [MTV Europe Music Awards 2020](https://pl.wikipedia.org/wiki/MTV_Europe_Music_Awards_2020)

**2019**

1. [Fryderyki 2020](https://pl.wikipedia.org/wiki/Fryderyki_2019)
2. [MTV Europe Music Awards 2019](https://pl.wikipedia.org/wiki/MTV_Europe_Music_Awards_2019)
3. [Przebój roku RMF FM](https://pl.wikipedia.org/wiki/Przeb%C3%B3j_roku_RMF_FM#2019_rok)

## Zdjęcie

![](https://img.redbull.com/images/c_crop,x_0,y_0,h_1849,w_2774/c_fill,w_1500,h_1000/q_auto,f_auto/redbullcom/2020/1/17/mb0nuzymniqkt6zeubkj/daria-zawialow-helsinki)
